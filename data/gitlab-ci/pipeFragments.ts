export const pipeFragments = [
  {
    uuid: 'd8be8429-f98d-4d8d-9b45-3ea156cc52e6',
    stages: ['test'],
    file: '/proxy/license-scanning.yml',
    tab: 'management',
    textContent: 'I want to track licensing',
    need: [],
    isSelected: false,
  },
  {
    uuid: 'b76b0429-d6cc-441f-8c63-484c905c69ae',
    stages: ['test'],
    file: '/proxy/code-quality.yml',
    tab: 'management',
    textContent:
      'I want to track code quality index (code climate) in pull requests. (⚠️ You will have an index, aim is to see an improvement or a breakdown, will not replace a linter.)',
    need: [],
    isSelected: false,
  },
  {
    uuid: 'c1f1051d-9280-4cfe-9ea4-c0e4e61cf77d',
    stages: ['test'],
    file: '/convco.auto.yml',
    tab: 'management',
    textContent:
      'I want to ensure that every commit is conventional commits formatted, to generate changelogs. (convco CLI is used)',
    need: [],
    isSelected: false,
  },
  {
    uuid: '16d6d6bd-08dd-471f-a3eb-8f8dd769ab2f',
    stages: ['docs'],
    file: '/mdbook.auto.yml',
    tab: 'management',
    textContent: 'I want to build documentation with mdbook',
    need: [],
    isSelected: false,
  },
  {
    uuid: '5ca98de8-9d68-4e60-9bbf-7b1c268aa5a7',
    stages: ['docs'],
    file: '/pandoc.#options.auto.yml',
    tab: 'management',
    options: [
      {
        label: 'all',
        value: 'all',
        query: 'all',
      },
      {
        label: 'docx',
        value: 'docx',
        query: 'docx',
      },
      {
        label: 'pdf',
        value: 'pdf',
        query: 'pdf',
      },
      {
        label: 'epub',
        value: 'epub',
        query: 'epub',
      },
    ],
    selectedOption: {
      label: 'all',
      value: 'all',
      query: 'all',
    },
    textContent: 'I want to build #options documentation with pandoc',
    need: [],
    isSelected: false,
  },
  {
    uuid: 'e8a5b09f-f0cd-4dba-acf9-72477a1ca53e',
    stages: ['build', 'release'],
    file: '/releaser.auto.yml',
    tab: 'management',
    textContent: 'I want to attach to tag a release with code',
    need: [],
    isSelected: false,
    variables: {
      GENERIC_PACKAGE_NAME: '${repositoryName}',
    },
  },
  {
    uuid: 'f6e447bd-4d20-4c15-a187-24054bd6345b',
    stages: ['build'],
    file: '/docker.auto.yml',
    tab: 'deployment',
    textContent: 'I want to build a docker image, and push it to gitlab registry.',
    need: [],
    isSelected: false,
  },
  {
    uuid: 'fb18a1e3-df07-42ab-9408-10d80f9a90c5',
    stages: ['deploy'],
    file: '/helm.reviews.#options.yml',
    tab: 'deployment',
    options: [
      {
        label: 'automatically',
        value: 'auto',
        query: 'review-auto',
      },
      {
        label: 'manually',
        value: 'manual',
        query: 'review-manual',
      },
    ],
    selectedOption: {
      label: 'automatically',
      value: 'auto',
      query: 'review-auto',
    },
    textContent:
      'I want to deploy reviews environments #options: an environment per branch other than the default branch. (Docker image should be in registry)',
    need: ['/docker.auto.yml'],
    isSelected: false,
    variables: {
      HOST: '${BASE_DOMAIN}',
      HELM_INGRESS_HOST: '${HOST}',
    },
  },
  {
    uuid: '45468327-3635-493f-accb-713fbd4527cc',
    stages: ['deploy'],
    file: '/helm.development.#options.yml',
    tab: 'deployment',
    textContent:
      'I want a development environment, #options synchronized with the default branch. (Docker image should be in registry)',
    need: ['/docker.auto.yml'],
    options: [
      {
        label: 'automatically',
        value: 'auto',
        query: 'develop-auto',
      },
      {
        label: 'manually',
        value: 'manual',
        query: 'develop-manual',
      },
    ],
    selectedOption: {
      label: 'automatically',
      value: 'auto',
      query: 'develop-auto',
    },
    isSelected: false,
    variables: {
      HOST: '${BASE_DOMAIN}',
      HELM_INGRESS_HOST: '${HOST}',
    },
  },
  {
    uuid: '78dd20c4-edf5-4022-aa88-239e07390107',
    stages: ['deploy'],
    file: '/helm.production.auto.yml',
    tab: 'deployment',
    textContent:
      'I want to publish to production environment on tagging. (Docker image should be in registry)',
    need: ['/docker.auto.yml'],
    isSelected: false,
    variables: {
      HOST: '${BASE_DOMAIN}',
      HELM_INGRESS_HOST: '${HOST}',
      HELM_NAME: '${repositoryName}',
    },
  },
  {
    uuid: '5ae68779-1ab9-4463-abb5-33e2e8ecb575',
    stages: ['test'],
    file: '/proxy/dependency-scanning.yml',
    tab: 'security',
    textContent: 'I want to check dependencies, to see vulnerabilities in merge requests.',
    need: [],
    isSelected: false,
  },
  {
    uuid: '64977e4f-dbd7-4385-af74-2047a3a153da',
    stages: ['test'],
    file: '/proxy/container-scanning.yml',
    tab: 'security',
    textContent:
      'I want to check softwares in docker image, to see vulnerabilities in merge requests. (Docker image should be in registry)',
    need: ['/docker.auto.yml'],
    isSelected: false,
  },
  {
    uuid: '3aff8d23-c789-4438-a2a0-3ff71b36d616',
    stages: ['test'],
    file: '/proxy/sast.yml',
    tab: 'security',
    textContent:
      'I want to analyse code, to see potential vulnerable patterns of code in merge requests.',
    need: [],
    isSelected: false,
  },
  {
    uuid: 'd84a578f-131e-4993-92ca-6c54a6f779b7',
    stages: ['test'],
    file: '/proxy/secret-scanning.yml',
    tab: 'security',
    textContent:
      "I want to check commits list, to detect leaked tokens / passwords in merge requests. When one is detected, I've to revoke and generate a new one",
    need: [],
    isSelected: false,
  },
  {
    uuid: '24104c7a-6803-4087-a3ed-1ff4f841953b',
    stages: ['dast'],
    file: '/proxy/dast.yml',
    tab: 'security',
    textContent:
      'I want automatic vulnerability scan over deployed environment, to see them in merge request. (Automatic deployments required)',
    need: ['/helm.reviews.auto.yml', '/helm.development.auto.yml', '/helm.production.auto.yml'],
    isSelected: false,
  },
  {
    uuid: 'c558f084-96b3-49c4-bebd-55a43a109780',
    stages: ['performance'],
    file: '/proxy/load-performance-testing.yml',
    tab: 'performance',
    textContent:
      'I want automatic server performance test over deployed environment, to see index in merge request. (Automatic deployments required)',
    need: ['/helm.reviews.auto.yml', '/helm.development.auto.yml', '/helm.production.auto.yml'],
    isSelected: false,
  },
  {
    uuid: '5e905a5a-5fcb-4166-a2d6-c00f787abb87',
    stages: ['performance'],
    file: '/proxy/browser-performance-testing',
    tab: 'performance',
    textContent:
      'I want automatic client performance test over deployed environment, to see and index in merge request (Automatic deployments required and file `.k6-test.js`)',
    need: ['/helm.reviews.auto.yml', '/helm.development.auto.yml', '/helm.production.auto.yml'],
    isSelected: false,
  },
];
