export const actionsAndConditionsList = [
  {
    textContent:
      '⚠️ I should contact the ops team to ensure that namespace exists, and gitlab repository/group is configured.',
    stage: 'deploy',
  },
  {
    textContent:
      '⚠️ I should ensure that any commit is well formatted (<a href="https://www.conventionalcommits.org/" target="_blank" rel="nofollow">Conventional commits</a>).',
    stage: 'test',
    file: '/convco.auto.yml',
  },
  {
    textContent:
      '️I have to create a <a href="https://k6.io/" target="_blank" rel="nofollow">`.k6-test.js`</a> file to the project\'s root. I can use the K6 generator.',
    stage: 'performance',
    file: '/proxy/browser-performance-testing.yml',
  },
  {
    textContent:
      "⚠️ I have to create a `Dockerfile` file to the project's root. I can use the Dockerfile generator.",
    stage: 'build',
    file: '/docker.auto.yml',
  },
  {
    textContent:
      '⚠️ The command `yarn test` will be triggered automatically, I should ensure that it exists.',
    stage: 'build',
    file: '/yarn.auto.yml',
  },
  {
    textContent:
      '⚠️ The command `npm test` will be triggered automatically, I should ensure that it exists.',
    stage: 'build',
    file: '/npm.auto.yml',
  },
  {
    textContent:
      "️️️⚠️ I should create a `.gitlab-ci.yml` file to the project's root, with the following content:",
  },
];
