export const dockerFileTemplate = ({
  runtimeVersion = 'lts',
  workDir = '/app',
  npmCacheDir = '/.npm',
  distroless = true,
}) => `FROM node:${runtimeVersion} as build
WORKDIR ${workDir}
COPY package.json ${workDir}
COPY package-lock.json ${workDir}
RUN npm ci --cache ${npmCacheDir}
COPY . .
RUN npm run build

FROM ${distroless ? `gcr.io/distroless/nodejs:${runtimeVersion}` : `node:${runtimeVersion}-slim`}
WORKDIR ${workDir}
COPY --from=build /app/.output /app/

EXPOSE 3000
ENV HOST=0.0.0.0
ENV PORT=3000

CMD [${!distroless ? '"node", ' : ''}"server/index.mjs"]
`;

export const dockerIgnoreTemplate = () => `.gitlab-ci.yml
.git
.gitignore
.vscode
.idea
Dockerfile*`;
