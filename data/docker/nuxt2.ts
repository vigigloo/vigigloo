export const dockerFileTemplate = ({
  runtimeVersion = 'lts',
  workDir = '/app',
  npmCacheDir = '/.npm',
  distroless = true,
}) => `FROM node:${runtimeVersion} as install
WORKDIR ${workDir}
COPY package.json ${workDir}
COPY package-lock.json ${workDir}
RUN npm ci --only=production --cache ${npmCacheDir}

FROM node:${runtimeVersion} as build
WORKDIR ${workDir}
COPY . .
COPY --from=install ${npmCacheDir} ${npmCacheDir}
RUN npm ci --cache ${npmCacheDir}
RUN npm run build

FROM ${distroless ? `gcr.io/distroless/nodejs:${runtimeVersion}` : `node:${runtimeVersion}-slim`}
WORKDIR ${workDir}
COPY --from=install /app/node_modules /app/node_modules
COPY --from=build /app/.nuxt /app/.nuxt
COPY --from=build /app/static /app/static

EXPOSE 3000
ENV HOST=0.0.0.0
ENV PORT=3000

CMD [${distroless ? `"node_modules/.bin/nuxt", "start"` : `"npm", "start"`}]
`;

export const dockerIgnoreTemplate = () => `.gitlab-ci.yml
.git
.gitignore
.vscode
.idea
Dockerfile*`;
