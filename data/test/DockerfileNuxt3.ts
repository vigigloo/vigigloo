export const dockerfile3 = `FROM node:lts AS build
ADD . /app
WORKDIR /app
RUN npm ci
RUN npm run build

FROM gcr.io/distroless/nodejs:lts
WORKDIR /app
COPY --from=build /app/.output/ /app/.output/

EXPOSE 3000
ENV PORT=3000
ENV HOST=0.0.0.0

CMD [ ".output/server/index.mjs" ]`;
