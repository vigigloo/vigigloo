FROM node:16 AS build
ADD . /app
WORKDIR /app
RUN npm ci
RUN npm run build

FROM gcr.io/distroless/nodejs:16
WORKDIR /app
COPY --from=build /app/.output/ /app/.output/

EXPOSE 3000
EXPOSE 9091
ENV HOST=0.0.0.0
ENV PORT=3000

CMD [ ".output/server/index.mjs" ]
