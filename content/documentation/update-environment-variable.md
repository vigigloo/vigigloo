```bash
#!/bin/bash

if [ -f "variables.auto.tfvars" ]; then
  read -p "'variables.auto.tfvars' already exists. Do you want to override it ? [y/N] " choice
  if [[ ! $choice =~ [yY] ]]; then
    exit 1
  fi
fi

: > variables.auto.tfvars

# Fetch CI environment variables via glab
ci_vars=$(glab api "projects/:fullpath/variables?filter[environment_scope]=*")

# Handle VAR_FILE separately if it exists
var_file_content=$(echo "$ci_vars" | jq -r '.[] | select(.key == "VAR_FILE") | .value')
if [ ! -z "$var_file_content" ]; then
  echo "$var_file_content" >> variables.auto.tfvars
fi

# Loop through variables and filter those that start with 'TF_VAR_', excluding VAR_FILE
echo $ci_vars | jq -rc '.[] | select(.key | startswith("TF_VAR_"))' | while read -r obj; do
  key=$(echo "$obj" | jq -r '.key')
  value=$(echo "$obj" | jq -r '.value')
  key=${key#TF_VAR_}

  if jq -e . >/dev/null 2>&1 <<<"$value"; then
    echo "$key = $value" >> variables.auto.tfvars
  else
    echo "$key = \"$value\"" >> variables.auto.tfvars
  fi
done
```