---
title: Features
position: 1
---

This section offers an overview of the features offered by the Vigiloo DevOps stack.

## Deployment

Projects can be deployed to several environments.
In practice, each environment is a distinct namespace in a Kubernetes cluster.

<img src="/environments-light.svg" class="light-img" alt="Diagram showing how the different environments are deployed to"/>

In GitLab, deployed environments are listed under **Deployments / Environments**.
The **Open** link on each environment points to the specific environment URL.

<img src="/gl-deploy-env-light.png" class="light-img" alt="Screenshot of the GitLab UI showing the deployed environments"/>

### Reviews

The reviews environment gets deployed to with every push to a repository branch that is not the default branch (usually `main`).
As such, there can be several versions of the same service deployed at the same time in the reviews environment.
Each has its own unique URL though.

### Development

This environment is also often called the staging environment.
Pushes to the default branch get deployed here.
As such, there is only one version of the service deployed.

This environment can be used to validate changes before deploying to production, as its configuration is the most comparable to the production environment.

### Production

This environment gets deployed to when tags are pushed.
The cluster used for production can be different from the development clusters.

## Modular CI file

In order to simplify CI setup for common uses cases, we use templates that we can compose together in a CI definition file.
There is [a generator for `.gitlab-ci.yml` files](https://vigigloo.fr/generator/gitlab-ci) that helps designing a CI pipeline interactively.
It covers the most common use-cases, but if you need additional tinkering you can check [the fragments documentation](https://vigigloo.gitlab.io/gitlab-pipeline-fragments/index.html).

You can also check out [the more detailed explanation](explanation.md#gitlab-ci-templates) if you want to learn about the reasons behind this structure.

## Helm charts

There are several helm charts that can be used to deploy your service depending on your needs.
The helm chart that will be used for a project depends on the tech stack used by the project, which is usually reflected in the Dockerfile.

### nodejs

The [nodejs chart](https://gitlab.com/vigigloo/tools/nodejs/-/tree/develop/helm_chart) is used for services that use node as web server.
It can be used for backends or frontends with SSR.

It is also possible to run one-time tasks during the deployment lifecycle.
This can be used for example to run migrations.

### nginx

The [nginx chart](https://gitlab.com/vigigloo/tools/nginx/-/tree/develop/helm_chart) is used for services that just need to serve static files like SPAs.

### migration

This [chart](https://gitlab.com/vigigloo/tools/migration/helm_chart) doesn't actually deploy a service that will be running, but instead launches containers that run a one-time task.
This can be used for example with a headless CMS like directus.
The repository would only contain the schema export and migrations, while the directus instance has been deployed as part of the infrastucture.

## Accessing cluster resources

While developping a project, it can be useful to have access to the deployed service or its infrastructure dependencies like databases.
It is possible to generate a config file containing credentials allowing access to only a restricted scope of the cluster.

Specific isntructions for different tasks can be found in the [How-to guides](howto.md).
