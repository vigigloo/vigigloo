Vigigloo is a DevOps stack.
It uses GitLab for CI/CD pipelines, deploys to a Kubernetes environment, and uses Terraform to manage infrastructure as code.

This stack is used within the [Fabrique Numérique Défense Connect](https://blog.beta.gouv.fr/categorie/fabnumdef/) and the [Incubateur des Territoires](https://incubateur.anct.gouv.fr/a-propos/).

You can find a brief description of what this stack is built for and the features it offers by looking at the [features](features.md) page.
Check out the [how to guides](howto.md) for performing specific tasks in your development workflow.
If you want to dive deeper in understanding the inner workings of this stack, there is [a more in-depth explanation](explanation.md).
And we also link to each tech's [reference documentation](reference.md).
