---
title: How-To guides
position: 2
---

These guides will help you perform a variety of common tasks.
There is also a [troubleshooting](howto.md#troubleshooting) section to help solve common issues.

## Adding environment variables to a deployed app

If you need to add environment variables to your deployed service, you can do so by going to the project's **Settings / CI/CD** page.
In the **Variables** section you can add an entry.

For the entry to be passed on to the container, it needs to be prefixed with `HELM_ENV_VAR_`.
For example, to add the `DEBUG` env var you'd add an entry called `HELM_ENV_VAR_DEBUG`.

You can also specify the environments for which this env var should be set by specifying an environment scope.
This would let you customize your service's config manually between reviews and development for example.

## Running migrations during deployment

When developping a node backend which is deployed using the [nodejs chart](features.md#nodejs), you can trigger specific actions during the deployment lifecycle.
Those are called hooks in Helm terminology.
The chart allows configuring these hooks easily by only passing a few parameters in YAML format.
Add this configuration to your project's `HELM_UPGRADE_VALUES` CI variable.

For example, then following hook definitions allow running TypeORM migrations during each install and upgrade.

```yaml
jobs:
  hooks:
    postInstall:
      - args: ["./node_modules/typeorm/cli.js", "migration:run"]
    preUpgrade:
      - args: ["./node_modules/typeorm/cli.js", "migration:run"]
```

An install is for the first service deployment, for example on a newly created branch in reviews or the first time the service runs in production.
An upgrade is for the subsequent deployments, where the new version comes replacing the previously deployed one.

Other configuration options can be found in [the chart's documentation](https://gitlab.com/vigigloo/tools/nodejs/-/blob/develop/helm_chart/README.md).

<!-- inform ops team of hook change? ask ops team to change hook? -->

## Using kubectl

If you haven't yet, [install kubectl](https://kubernetes.io/docs/tasks/tools/).

When [requesting access to cluster resources](features.md#accessing-cluster-resources) you'll be given a series of kubectl config files, one for each environment.
There are several ways to tell kubectl which config file to use, but we'll highlight the most straightforward ones:
- You can use the `--kubeconfig <path/to/file>` option when running kubectl to run a single command with this config
- You can set the `KUBECONFIG` environment variable to point to your config file if you want to use it for all kubectl calls in your current shell

After setting up your config, you can run `kubectl get pods` to show what's running in the environment.
Example output:

```
NAME                              READY   STATUS      RESTARTS   AGE
backend-nodejs-59fb79cb6d-j8qc4   1/1     Running     0          74m
frontend-nginx-5c65645d7-sg89q    1/1     Running     0          76m
postgresql-instance1-22qq-0       1/1     Running     0          11d
```

Finally, you can [enable autocompletion for your shell](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#optional-kubectl-configurations-and-plugins) to make it easier to user.

## Checking app logs

You'll need to [configure kubectl](howto.md#using-kubectl) if you haven't already.

You can read your service's logs using the `kubectl logs` command.
It requires the name of the Pod you want to read logs from.

```
$ kubectl get pods
NAME                              READY   STATUS      RESTARTS   AGE
backend-nodejs-59fb79cb6d-j8qc4   1/1     Running     0          74m
frontend-nginx-5c65645d7-sg89q    1/1     Running     0          76m
postgresql-instance1-22qq-0       1/1     Running     0          11d
$ kubectl logs backend-nodejs-59fb79cb6d-j8qc4
...
```

It is also possible to keep streaming the logs instead of returning after the last line with the `-f` flag:
```
$ kubectl logs backend-nodejs-59fb79cb6d-j8qc4 -f
...
```

Accessing logs this way is only possible while the Pod is still showing up when running `kubectl get pods`.
It won't work if the Pod isn't shown anymore, for example when it's been replaced by a new version.

## Accessing a database in the cluster

You'll need to [configure kubectl](howto.md#using-kubectl) if you haven't already.

You can access a database running in the cluster by making it available at `localhost` using port-forwarding.
For this, you'll need to know the name of the Pod running the database and the port it's exposing.

```
$ kubectl get pods
NAME                              READY   STATUS      RESTARTS   AGE
backend-nodejs-59fb79cb6d-j8qc4   1/1     Running     0          74m
frontend-nginx-5c65645d7-sg89q    1/1     Running     0          76m
postgresql-instance1-22qq-0       1/1     Running     0          11d
$ kubectl port-forward postgresql-instance1-22qq-0 5432
Forwarding from 127.0.0.1:5432 -> 5432
```

Database credentials, among others, are kept in Secrets.

```
$ kubectl get secrets
NAME                                TYPE                DATA   AGE
backend-nodejs-tls                  kubernetes.io/tls   2      41d
frontend-nginx-tls                  kubernetes.io/tls   2      41d
postgresql-instance1-22qq-certs     Opaque              4      11d
postgresql-pguser-postgresql        Opaque              8      29d
...
```

In this example the credentials are in the `postgresql-pguser-postgresql` Secret.
To show the content of a secret, you can use the `-o json` or `-o yaml` options.

```
$ kubectl get secrets postgresql-pguser-postgresql -o yaml
apiVersion: v1
data:
  dbname: cG9zdGdyZXNxbA==
  password: YnR3IGkgdXNlIGFyY2g=
  user: cG9zdGdyZXNxbA==
  ...
kind: Secret
metadata:
  ...
type: Opaque
```

The field values are encoded in base64, so you'll need to pipe them through `base64 -d` before using them.

We can now access our database:

```
$ psql -h localhost -U postgres -p 5432
Password for user postgresql:
```

## Running a shell in a container

You'll need to [configure kubectl](howto.md#using-kubectl) if you haven't already.

You can also spawn a shell in a Pod's container for debugging.
Note however that containers based on [distroless images](https://github.com/GoogleContainerTools/distroless) do not allow this to improve security.

```
# Running a command one time, useful in scripts
$ kubectl exec postgresql-instance1-22qq-0 -- sh -c 'psql --version'
Defaulted container "database" out of: database, replication-cert-copy, pgbackrest, postgres-startup (init), nss-wrapper-init (init)
psql (PostgreSQL) 13.5
# Running sh in an interactive shell
$ kubectl exec -it postgresql-instance1-22qq-0 -- sh
Defaulted container "database" out of: database, replication-cert-copy, pgbackrest, postgres-startup (init), nss-wrapper-init (init)
sh-4.4$
```

## Troubleshooting

This section lists common issues that can be encountered and their solutions or workarounds.
For most of them it is recommended to [configure kubectl](howto.md#using-kubectl) beforehand.

### Jobs failing because of runner issues

A CI job might fail with an error similar to this:

```
ERROR: Error cleaning up pod: rpc error: code = Unavailable desc = error reading from server: read tcp 100.66.22.112:52866->62.210.18.120:2379: read: connection timed out
ERROR: Error cleaning up configmap: resource name may not be empty
ERROR: Job failed (system failure): prepare environment: setting up build pod: error setting ownerReferences: rpc error: code = Unavailable desc = error reading from server: read tcp 100.66.22.112:50566->62.210.18.120:2379: read: connection timed out. Check https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading for more information
```

This is caused by a temporary runner issue, and the failing job can be restarted manually to make the pipeline progress further.

### Deployment failing because of migrations

If an error happens while running the migrations, they will be retried a few times.
If none completed successfully, the deployment fails with the following error:

```
Error: UPGRADE FAILED: release backend failed, and has been rolled back due to atomic being set: pre-upgrade hooks failed: job failed: BackoffLimitExceeded
```

You can check the logs from the pod running the migrations for more details.

```
$ kubectl get jobs
NAME             COMPLETIONS   DURATION   AGE
backend-nodejs   0/1           23s        23s
...
$ kubectl get pods
NAME                              READY   STATUS   RESTARTS   AGE
backend-nodejs-4g5vg              0/1     Error    0          23s
...
$ kubectl logs backend-nodejs-4g5vg
...
```

### Migrations timeout

In some circumstances, the helm hooks used to run migrations can take a very long time to complete.
This can cause the deployment to timeout with an error similar to this:

```
Error: release backend failed, and has been uninstalled due to atomic being set: failed post-install: timed out waiting for the condition
```
```
Error: UPGRADE FAILED: release backend failed, and has been rolled back due to atomic being set: pre-upgrade hooks failed: timed out waiting for the condition
```

Using kubectl, you can see the job stuck:
```
$ kubectl get pods
NAME                   READY   STATUS      RESTARTS   AGE
backend-nodejs-4g5vg   0/1     Completed   0          3m27s
...
# The pod seems to have completed its task
$ kubectl get jobs
NAME             COMPLETIONS   DURATION   AGE
backend-nodejs   0/1           3m27s      4m37s
...
# The job is still waiting, it didn't notice the pod's completion
```

A temporary workaround would be to increase the timeout value, by adding a `HELM_TIMEOUT` variable to the project's CI variables.
For example, set it to `10m0s` if you want to increase the timeout to 10 minutes instead of the default 5 minutes.

### Limited by resources in reviews

In the reviews environment a deployment might fail and show an error looking like one of these:

```
Error: release backend failed, and has been uninstalled due to atomic being set: timed out waiting for the condition
```
```
Error: UPGRADE FAILED: release backend failed, and has been rolled back due to atomic being set: timed out waiting for the condition
```

Each kubernetes namespace has resource quotas to prevent buggy deployments from spawing more Pods that could then trigger costly cluster scale-ups.
In the reviews environment, as each branch deploys its own version, there can be many Pods at the same time filling the resource quota.
When that happens, the new Pod will never be created.

You can check the reason for the Pod not being created during a deployment by looking at the most recent ReplicaSet:

```
$ kubect get replicaset
NAME                                   DESIRED   CURRENT   READY   AGE
frontend-feat-something-5ddb748d99     1         1         1       22d
frontend-feat-improvement-76ff9d6d76   1         1         1       22d
frontend-fix-config-7bbb9b5f49         1         0         0       48s
...
$ kubectl describe replicaset frontend-fix-config-7bbb9b5f49
```

The last lines of the `describe` output would mention the resource quota being exceeded.

You can manually stop unnecessary environments in GitLab's **Deployments / Environments** page.

### Failed deploy because of pending install

The following error can show up when a previous deployment failed abruptly, for example because of a runner failure.

```
Error: UPGRADE FAILED: another operation (install/upgrade/rollback) is in progress
```

The deployment is left in an unrecoverable state.
To progress further, stop the environment in GitLab's **Deployments / Environments** page.
This should trigger the `deploy:<environment name>:uninstall` job, after which you can deploy again.
If the uninstall job didn't trigger when stopping the environment, you can also trigger it manually from the pipeline.

### Removing environments manually

In some rare cases, Gitlab fails to actually perform the uninstall when removing an environment in the reviews namespace.
This then requires a manual cleanup using [Helm](reference.md#helm).

List the installed releases using `helm list`:
```
NAME               NAMESPACE      REVISION  UPDATED                                  STATUS    CHART           APP VERSION
directus           myapp-reviews  2         2023-01-16 16:19:33.30920201 +0100 CET   deployed  directus-0.4.0  9.7.1
frontend-feat-foo  myapp-reviews  4         2023-02-17 12:49:02.234698991 +0000 UTC  deployed  nodejs-0.1.1
frontend-fix-bar   myapp-reviews  4         2023-02-17 12:59:01.134098019 +0000 UTC  deployed  nodejs-0.1.1
```

Uninstall a release using `helm uninstall <release name>`.

<!-- ### Long branch names -->
<!-- long branch names fail with this msg -->
<!-- use shorter branch names -->
