---
title: Explanation
position: 3
---

This chapter aims to provide an in-depth insight of how the different components of the stack are used.
It gives an architectural overview that can be useful for someone who wants to have a deeper understanding.
This is also a good place to find leads if you want to contribute to a particular component or if you want to be able to tweak your development process to better accomodate your needs.

## Overview

The following diagram provides an overview of how the different repositories relate to each other in the DevOps process.

<a href="overview-light.svg" class="light-img" target="blank"><img src="/overview-light.svg" class="light-img" alt="Diagram showing how the different projects relate to each other in the DevOps stack"/></a>

### Infrastructure project

The infrastructure projects are the root of the whole infrastructure.
They use Terraform to set up the Kubernetes clusters, and configure a variety of resources.

Here is a non-exhaustive list of things it manages:
- Cluster-wide tooling
  - CertManager, to allow automated TLS certificates renewal
  - PostgreSQL Operator, to manage PostgreSQL deployments
  - HAProxy, the ingress controller
- Services used across several teams
  - GitLab runners for all teams terraform projects
  - Chat platform like Mattermost
- Various specific configurations
  - Domain names for projects deployed outside the Kubernetes cluster
  - Redirections for legacy projects
  - Object buckets

Using the infrastructure project, [team-specific infrastructure projects](explanation.md#team-infrastructure-projects) can also be created using [the GitLab Terraform project module](https://gitlab.com/vigigloo/tf-modules/gitlab-terraform-project) from a [template](https://gitlab.com/vigigloo/templates/terraform-project).

Here are the links to the infrastructure projects:
- [Fabrique Numérique Défense Connect](https://gitlab.com/fabnum-minarm/igloo/terraform/main)
- [Incubateur des Territoires](https://gitlab.com/incubateur-territoires/devops/infrastructure/)

### Team infrastructure projects

The team infrastructure projects are used to manage everything that should be scoped to only one team.
They also use terraform for the following purposes:
- Setting up GitLab runners for the team projects
- Creating Kubernetes namespaces
- Setting up subdomains for the team's deployed services
- Deploying infrastructure-related services like database or vendored services like Keycloak
- Configuring [project repositories](explanation.md#team-application-projects) for deployment

Here are the links to the groups containing the team infrastructure projects:
- [Fabrique Numérique Défense Connect](https://gitlab.com/fabnum-minarm/igloo/terraform/projects)
- [Incubateur des Territoires](https://gitlab.com/incubateur-territoires/devops/projects)

### Team application projects

These repositories are the ones most close to the development teams.
They contain their own sources and use a `.gitlab-ci.yml` file to build a docker image and deploy the service.
The deployment relies on CI variables that are configured by the [team infrastructure project](explanation.md#team-infrastructure-projects).

Here are the links to the groups:
- [Fabrique Numérique Défense Connect](https://gitlab.com/fabnum-minarm)
- [Incubateur des Territoires](https://gitlab.com/incubateur-territoires/startups)

## Other repositories

The various repositories listed below are used at different parts of the DevOps process.
They are mentionned aside because their function is more specific to a particular task.

<!-- ### Helm charts -->
<!-- link vigigloo tools nodejs and nginx -->
<!-- used for deploying generic apps developed by teams -->

### GitLab CI templates

The [GitLab CI fragments repository](https://gitlab.com/vigigloo/gitlab-pipeline-fragments) provides templates that can be used to easily compose a `.gitlab-ci.yml` file with a specific set of features.
These templates are used both in infrastructure repositories and in applicative project repositories.

There are usually two types of fragments: pure templates, and default preconfigured jobs.

In GitLab CI syntax, a job with a name starting with a dot (`.`) is a hidden job, which means it won't run at all and is only used for templating.
Such a job can then be extended by another job, making it possible to adapt the job's configuration at the same time.
If the extending job doesn't start with a dot it will be able to run.

This workflow answers two needs:
- Most common job definitions can be made as a fragment that can then be included in a CI file with minimal configuration. This can accomodate several projects.
- Projects with specific needs can import just parts of the template directly to avoid having to redesign all the CI details.

### Vigigloo tools

The [tools GitLab group](https://gitlab.com/vigigloo/tools) is where every third-party service is prepared for deployment.
This usually includes Helm charts and Terraform modules, or only the Terraform module when an existing Open Source chart looks well maintained.
The Terraform module is often a wrapper deploying the Helm chart with most common parameters.

For example [Directus](https://gitlab.com/vigigloo/tools/directus), [Elasticsearch](https://gitlab.com/vigigloo/tools/elasticsearch/) or [PostgreSQL](https://gitlab.com/vigigloo/tools/postgresql/) all have their repository in tools to simplify their deployment.

### Vigigloo terraform modules

[Custom terraform modules](https://gitlab.com/vigigloo/tf-modules/) are sometimes designed to answer to a frequent configuration need.
They are used throughout the infrastructure repositories.
