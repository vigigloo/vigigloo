---
title: Reference
position: 4
---

This page links to the various reference documentations of each technology used in Vigigloo.

## Kubernetes

- [Kubernetes API reference](https://kubernetes.io/docs/reference/kubernetes-api/)
- [Installing kubectl CLI](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- [kubectl cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- [kubectl CLI reference](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands)

## GitLab CI

- [Syntax reference](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [GitLab CI predefined variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

## Helm

- [Helm template functions](https://helm.sh/docs/chart_template_guide/function_list/)
- [Installing helm CLI](https://helm.sh/docs/intro/install/)
- [helm CLI reference](https://helm.sh/docs/helm/helm/)

## Terraform

- [Terraform language reference](https://www.terraform.io/language)
- [Installing terraform CLI](https://developer.hashicorp.com/terraform/downloads)
- [Terraform CLI reference](https://www.terraform.io/cli/commands)
- [Terraform GitLab provider documentation](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs)
- [Terraform Helm provider documentation](https://registry.terraform.io/providers/hashicorp/helm/latest/docs)
- [Terraform Kubernetes provider documentation](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs)
- [Terraform Scaleway provider documentation](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs)
