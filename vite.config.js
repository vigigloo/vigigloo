import path from 'path';
import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    include: ['./composables/*.spec.ts'],
    testTimeout: 15000,
  },
  resolve: {
    alias: {
      '~': path.resolve(__dirname, './'),
    },
  },
});
