export interface LabelInputData {
  title: string;
  id?: string;
  target?: string;
}

export interface CheckboxDataTag {
  title: string;
  name: string;
  labelInput?: LabelInputData;
}

export interface ActionAndConditions {
  textContent: string;
  stage?: string;
  file?: string;
}

export class CheckboxDataTags extends Array<CheckboxDataTag> {}
