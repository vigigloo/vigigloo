export enum PACKAGE_MANAGER {
  NPM = 'npm',
  YARN = 'yarn',
}

export enum PRISM_LANGUAGE {
  YAML = 'yaml',
  DOCKERFILE = 'dockerfile',
  IGNORE = 'ignore',
}

export enum LANGUAGE {
  JS = 'Javascript/NodeJS',
  DENO = 'Javascript/Deno',
}
export const languages = Object.values(LANGUAGE);

export enum NODE_VERSION {
  LTS = 'lts',
  V14 = '14',
  V16 = '16',
  V18 = '18',
}

export enum DENO_VERSION {
  _ = '_',
}
export type RuntimeVersion = NODE_VERSION | DENO_VERSION;

export const availablePackageManagers = (language: string): Record<string, string> => {
  switch (language) {
    case LANGUAGE.JS:
      return {
        npm: PACKAGE_MANAGER.NPM,
        yarn: PACKAGE_MANAGER.YARN,
      };
  }
};

interface IConf {
  language: LANGUAGE | '';
  packageManager: PACKAGE_MANAGER | '';
}
export class Conf implements IConf {
  language: LANGUAGE | '' = '';
  packageManager: PACKAGE_MANAGER | '' = '';
}

interface IGitlabConf extends IConf {
  projectName: string;
  repositoryName: string;
  subdomain: string;
  version: string;
}
export class GitlabConf extends Conf implements IGitlabConf {
  projectName = '';
  repositoryName = '';
  subdomain = '';
  version = '';
}

interface IDockerConf extends IConf {
  runtimeVersion: RuntimeVersion | '';
  scriptName: string;
  framework: string;
  isNodeModuleRequiredInRuntime: boolean;
  isDistroless: boolean;
  areDependenciesSplit: boolean;
  exposedPort?: string;
  prometheusPort?: string;
}

export class DockerConf extends Conf implements IDockerConf {
  scriptName = '';
  framework = '';
  runtimeVersion: RuntimeVersion | '' = '';
  isNodeModuleRequiredInRuntime = false;
  isDistroless = false;
  areDependenciesSplit = false;
  exposedPort = '';
  prometheusPort = '';
}

export type DockerConfKeys = keyof IDockerConf;

export type ConfBatch = IDockerConf | IGitlabConf;
