export class PipeOption {
  label: string;
  value: string;
  query: string;
}
export class PipeOptions extends Array<PipeOption> {}

export interface PipeFragment {
  uuid: string;
  file: string;
  textContent: string;
  tab: string;
  stages?: string[];
  need?: string[];
  isSelected?: boolean;
  variables?: Record<string, string>;
  options?: PipeOptions;
  selectedOption?: PipeOption;
}

export interface PipeFragments extends Array<PipeFragment> {}
