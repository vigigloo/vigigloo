import { defineNuxtConfig } from 'nuxt/config';

export default defineNuxtConfig({
  ssr: false,
  modules: ['@nuxt/content'],
  content: {
    navigation: {
      fields: ['title', 'position'],
    },
  },
  head: {
    title: 'Vigigloo docs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
  },
  css: ['@/assets/scss/style.scss'],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          sourceMap: true,
          additionalData: `@import "@/assets/scss/abstracts/_colors.scss"; `,
        },
      },
    },
  },
});
