import { ComputedRef, Ref } from 'nuxt/dist/app/compat/capi';
import { ConfBatch } from '~/models/configuration';

export default function useUrlLib() {
  const route = useRoute();
  const router = useRouter();

  type QueryData = Ref<ConfBatch> | ComputedRef<{ [K in 'files' | 'options']?: string[] }>;
  const buildQueryParams = (obj: QueryData) => {
    return Object.entries(obj).reduce((query, [key, value]) => {
      if (value && (typeof value === 'boolean' || value.length > 0)) {
        query[key] = encodeURIComponent(value.toString());
      }
      return query;
    }, {} as Record<string, string>);
  };

  const getQueryParams = (newRefs: QueryData[]) => {
    watch(
      newRefs,
      () => {
        const refValues = Object.entries(newRefs).reduce((array, [_, val]) => {
          return { ...array, ...val.value };
        }, {} as QueryData);

        router.push({
          path: route.fullPath,
          query: buildQueryParams(refValues),
        });
      },
      { deep: true },
    );
  };

  const useQueryParams = (newRef: QueryData, callback?: Function) => {
    onMounted(() => {
      const query = route.query;
      return Object.keys(newRef.value).reduce((_, key) => {
        if (typeof newRef.value[key] === 'boolean') {
          newRef.value[key] = query[key] === 'true';
        } else if (query[key] && typeof newRef.value[key] === 'string') {
          newRef.value[key] = decodeURIComponent(String(query[key]));
        }
        callback && callback(query);
        return newRef;
      }, {});
    });
  };

  return {
    getQueryParams,
    useQueryParams,
  };
}
