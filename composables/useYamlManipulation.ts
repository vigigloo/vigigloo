import { dash } from 'any-case';
import { dump } from 'js-yaml';
import { GitlabConf, LANGUAGE } from '~/models/configuration';
import { PipeFragments } from '~/models/gitlab-ci';

export default function useYamlManipulation() {
  function createYamlObject(conf: GitlabConf, pipeFragments: PipeFragments): Record<string, any> {
    let stages = new Set();
    const files: Record<string, Set<string>> = {};
    let variables: Record<string, string> = {};

    if (conf.language === 'Javascript/NodeJS' && conf.packageManager) {
      files.build = files.build || new Set();
      files.build.add('/' + conf.packageManager + '.auto.yml');
      stages.add('build');
    }

    if (conf.language === 'Javascript/Deno') {
      files.build = files.build || new Set();
      files.build.add('/deno.auto.yml');
      stages.add('build');
      stages.add('test');
      stages.add('release');
    }

    if (pipeFragments) {
      pipeFragments.forEach((element) => {
        if (element.isSelected) {
          stages = new Set([...element.stages, ...stages]);

          const stage = element.stages[0];
          files[stage] = files[stage] || new Set();

          let file = element.file;

          if (element.selectedOption) {
            file = file.replace('#options', element.selectedOption?.value);
          }

          files[stage].add(file);

          if (element.variables) {
            variables = { ...element.variables, ...variables };
          }

          if (conf.subdomain) {
            const helmHost = {
              HOST: '${subdomainPrefix}.${BASE_DOMAIN}',
            };
            variables = { ...variables, ...helmHost };
          }

          if (element.file.includes('helm')) {
            switch (conf.language) {
              case LANGUAGE.JS: {
                const helmNode = {
                  HELM_CHART: 'nodejs',
                  HELM_REPOSITORY:
                    'https://gitlab.com/api/v4/projects/29298483/packages/helm/stable',
                };
                variables = { ...variables, ...helmNode };
              }
            }
          }
        }
      });

      const yamlfiles = Object.entries(files).reduce((array, [key, val]) => {
        return [...array, 'Comment ' + key, ...val];
      }, []);

      return {
        stages: [...stages].sort(),
        include: {
          project: 'vigigloo/gitlab-pipeline-fragments',
          file: yamlfiles,
        },
        variables,
      };
    }
  }

  function yamlContent(conf: GitlabConf, pipeFragments: PipeFragments): string {
    const yaml = createYamlObject(conf, pipeFragments);
    const repoName = dash(conf.repositoryName);
    const subdomain = dash(conf.subdomain);

    function replacer(_, value) {
      if (JSON.stringify(value) === '[]' || JSON.stringify(value) === '{}') {
        return undefined;
      }
      return value;
    }

    return (
      `# .gitlab-ci.yml for ${conf.projectName} (${conf.repositoryName}) \n` +
      dump(yaml, { replacer })
        .replace(/\${subdomainPrefix}/g, subdomain)
        .replace(/\${repositoryName}/g, repoName)
        .replace(/- Comment/g, '# ')
    );
  }

  return { createYamlObject, yamlContent };
}
