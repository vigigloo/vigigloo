import { describe, test, expect, beforeAll } from 'vitest';
import useYamlManipulation from './useYamlManipulation';
import { pipeFragments } from '~/data/gitlab-ci/pipeFragments';
import { GitlabConf, PACKAGE_MANAGER } from '~/models/configuration';
const { createYamlObject, yamlContent } = useYamlManipulation();

describe('Given all pipe fragments unselected and configuration empty', () => {
  let pipeFragmentsForTest;
  let conf;

  beforeAll(() => {
    pipeFragmentsForTest = pipeFragments;
    conf = new GitlabConf();
  });

  test('When project and repository are set', () => {
    conf.projectName = 'Test';
    conf.repositoryName = 'Repo';

    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: [],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: [],
      },
      variables: {},
    });
    expect(yamlContent(conf, pipeFragmentsForTest)).toMatch(/# \.gitlab-ci.yml for Test \(Repo\)/);
  });

  test('When selected deno', () => {
    conf.language = 'Javascript/Deno';
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['build', 'release', 'test'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment build', '/deno.auto.yml'],
      },
      variables: {},
    });
  });

  test('When selected npm packpage manager', () => {
    conf.language = 'Javascript/NodeJS';
    conf.packageManager = PACKAGE_MANAGER.NPM;
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['build'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment build', '/npm.auto.yml'],
      },
      variables: {},
    });
  });

  test('When selected license-scanning fragment', () => {
    pipeFragmentsForTest[0].isSelected = true;
    conf.packageManager = '';
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['test'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment test', '/proxy/license-scanning.yml'],
      },
      variables: {},
    });
  });

  test('When selected mdbook fragment', () => {
    pipeFragmentsForTest[0].isSelected = false;
    pipeFragmentsForTest[3].isSelected = true;
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['docs'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment docs', '/mdbook.auto.yml'],
      },
      variables: {},
    });
  });

  test('When selected pandoc (all) fragment', () => {
    pipeFragmentsForTest[3].isSelected = false;
    pipeFragmentsForTest[4].isSelected = true;
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['docs'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment docs', '/pandoc.all.auto.yml'],
      },
      variables: {},
    });
  });

  test('When selected pandoc (pdf) fragment', () => {
    pipeFragmentsForTest[3].isSelected = false;
    pipeFragmentsForTest[4].isSelected = true;
    pipeFragmentsForTest[4].selectedOption = {
      query: 'pdf',
      value: 'pdf',
      label: 'pdf',
    };
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['docs'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment docs', '/pandoc.pdf.auto.yml'],
      },
      variables: {},
    });
  });

  test('When selected releaser fragment', () => {
    pipeFragmentsForTest[4].isSelected = false;
    pipeFragmentsForTest[5].isSelected = true;
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['build', 'release'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment build', '/releaser.auto.yml'],
      },
      variables: {
        GENERIC_PACKAGE_NAME: '${repositoryName}',
      },
    });
  });

  test('When selected Docker.auto fragment', () => {
    pipeFragmentsForTest[5].isSelected = false;
    pipeFragmentsForTest[6].isSelected = true;

    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['build'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment build', '/docker.auto.yml'],
      },
      variables: {},
    });
  });

  test('When selected Docker.auto and helm.reviews.auto fragments and no subdomain defined', () => {
    pipeFragmentsForTest[6].isSelected = true;
    pipeFragmentsForTest[7].isSelected = true;
    pipeFragmentsForTest[4].selectedOption = {
      label: 'automatically',
      value: 'auto',
      query: 'review-auto',
    };

    conf.language = 'Javascript/NodeJS';
    conf.PACKAGE_MANAGER = PACKAGE_MANAGER.YARN;

    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['build', 'deploy'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment build', '/docker.auto.yml', 'Comment deploy', '/helm.reviews.auto.yml'],
      },
      variables: {
        HELM_CHART: 'nodejs',
        HELM_INGRESS_HOST: '${HOST}',
        HELM_REPOSITORY: 'https://gitlab.com/api/v4/projects/29298483/packages/helm/stable',
        HOST: '${BASE_DOMAIN}',
      },
    });
  });

  test('When selected helm.reviews.auto fragments and defined a subdomain', () => {
    pipeFragmentsForTest[6].isSelected = false;
    pipeFragmentsForTest[7].isSelected = true;
    pipeFragmentsForTest[4].selectedOption = {
      label: 'automatically',
      value: 'auto',
      query: 'review-auto',
    };

    conf.subdomain = 'suddomain- test';
    expect(createYamlObject(conf, pipeFragmentsForTest)).toStrictEqual({
      stages: ['deploy'],
      include: {
        project: 'vigigloo/gitlab-pipeline-fragments',
        file: ['Comment deploy', '/helm.reviews.auto.yml'],
      },
      variables: {
        HELM_INGRESS_HOST: '${HOST}',
        HELM_CHART: 'nodejs',
        HOST: '${subdomainPrefix}.${BASE_DOMAIN}',
        HELM_REPOSITORY: 'https://gitlab.com/api/v4/projects/29298483/packages/helm/stable',
      },
    });
  });
});
