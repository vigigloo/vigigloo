export default function useStrManipulation() {
  function camelize(str): string {
    return str.replace(/-./g, (x) => x[1].toUpperCase());
  }

  function capitalize(str): string {
    return str[0].toUpperCase() + str.substr(1);
  }

  return { capitalize, camelize };
}
